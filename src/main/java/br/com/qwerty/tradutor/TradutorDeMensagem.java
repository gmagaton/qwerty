package br.com.qwerty.tradutor;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

/**
 * Serviço de tradução.
 *
 * Mensagem para código {@link #daMensagemParaCodigo(String)} Código para
 * mensagem {@link #doCodigoParaMensagem(String)}
 * <p>
 * Exemplo:
 * <p>
 *
 * tradutor.daMensagemParaCodigo("B") retornará 22
 * tratudor.doCodigoParaMensagem("22") retornará "B
 *
 * @author Gabriel
 *
 */
public class TradutorDeMensagem {

    private static final String separadorDeMesmaTecla = "_";

    private final String[] alfabeto = { " ", "A", "B", "C", "D", "E", "F", "G",
	    "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
	    "U", "V", "W", "X", "Y", "Z" };
    private final static String[] numeros = { "0", "2", "22", "222", "3", "33",
	    "333", "4", "44", "444", "5", "55", "555", "6", "66", "666", "7",
	    "77", "777", "7777", "8", "88", "888", "9", "99", "999", "9999" };

    /*
     * Traduz a mensagem para os números que devem ser digitados para escrever a
     * mensagem
     * 
     * @param mensagem {@link String} mensagem de texto que deseja escrever
     * 
     * @return {@link String} sequência numérica correspondente a mensagem
     */
    private String daMensagemParaCodigo(final String mensagem) {
	final StringBuilder mensagemTraduzida = new StringBuilder();
	String ultimoValor = null;
	for (int i = 0; i < mensagem.length(); i++) {
	    final Character caractere = mensagem.charAt(i);

	    final int indice = Arrays.binarySearch(alfabeto, caractere
		    .toString().toUpperCase());
	    final String valor = numeros[indice];

	    if (ultimoValor != null
		    && new Character(valor.charAt(0)).equals(new Character(
			    ultimoValor.charAt(0)))) {
		mensagemTraduzida.append(separadorDeMesmaTecla);
	    }
	    ultimoValor = valor;

	    mensagemTraduzida.append(valor);
	}

	return mensagemTraduzida.toString();
    }

    /*
     * Traduz o código em mensagem de texto
     * 
     * @param codigo {@link String} sequência de código digitado
     * 
     * @return {@link String} mensagem gerada a partir do código
     */
    private String doCodigoParaMensagem(final String codigo) {
	final StringBuilder mensagemTraduzida = new StringBuilder();
	Character caractereAnterior = null;
	StringBuilder conjuntoNumerico = new StringBuilder();

	for (int i = 0; i < codigo.length(); i++) {

	    final Character caractere = Character.toUpperCase(codigo.charAt(i));
	    if (caractereAnterior != null
		    && !caractere.equals(caractereAnterior)) {
		agregarConjutoNumerico(mensagemTraduzida, conjuntoNumerico);
		conjuntoNumerico = new StringBuilder();
	    } else {
		conjuntoNumerico.append(caractere.toString());
	    }
	    caractereAnterior = caractere;

	}

	agregarConjutoNumerico(mensagemTraduzida, conjuntoNumerico);

	return mensagemTraduzida.toString();
    }

    private void agregarConjutoNumerico(final StringBuilder mensagemTraduzida,
	    final StringBuilder conjuntoNumerico) {
	final int indice = Arrays.binarySearch(numeros,
		conjuntoNumerico.toString());
	final String valor = alfabeto[indice];
	mensagemTraduzida.append(valor);
    }

    /**
     * Traduz uma mensagem ou sequencia de teclas
     *
     * @param mensagemOuCodigo
     *            {@link String} mesangem ou sequência de teclas
     * @return {@link String} tradução
     */
    public String traduzir(final String mensagemOuCodigo) {
	if (StringUtils.isBlank(mensagemOuCodigo)) {
	    return "";
	}
	// texto ou codigo
	final boolean numerico = StringUtils.isNumeric(mensagemOuCodigo
		.replace(separadorDeMesmaTecla, ""));

	if (numerico) {
	    return doCodigoParaMensagem(mensagemOuCodigo);
	} else {
	    return daMensagemParaCodigo(mensagemOuCodigo);
	}
    }
}
