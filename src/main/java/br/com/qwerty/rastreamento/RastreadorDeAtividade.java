package br.com.qwerty.rastreamento;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Rastreador de Atividades
 *
 * Responsável por registrar todas as operações de tradução das mensagens SMS
 *
 * FIXME: Atentar para leitura/escrita contorrente no arquivo
 *
 * @author Gabriel
 *
 */
public class RastreadorDeAtividade {

    private static final String arquivoDeAtividades = "atividades.txt";
    private static final String separadorDeLinha = System
	    .getProperty("line.separator");
    private static final String separadorUsuarioRegistro = "&";
    private static final String ROOT = "ROOT";

    private File arquivo = null;

    public RastreadorDeAtividade() {
	final String path = new File(".").getAbsolutePath();

	final File diretorio = new File(path);
	arquivo = new File(diretorio, arquivoDeAtividades);
	try {
	    arquivo.createNewFile();
	} catch (final IOException e) {
	    throw new RuntimeException(e);
	}

    }

    /**
     * Registra a atividade de um usuário
     *
     * @param usuario
     *            {@link String} usuário que esta executando
     *
     * @param registro
     *            {@link String} mensagem ou codigo executado
     */
    public void registrar(final String usuario, final String registro) {
	if (StringUtils.isBlank(usuario) || StringUtils.isBlank(registro)) {
	    return;
	}
	try {
	    FileUtils.write(arquivo, usuario, true);
	    FileUtils.write(arquivo, separadorUsuarioRegistro, true);
	    FileUtils.write(arquivo, registro, true);
	    FileUtils.write(arquivo, separadorDeLinha, true);
	} catch (final IOException e) {
	    throw new RuntimeException(e);
	}
    }

    /**
     * Lista as atividades(código/mensagem) já executadas pelo usuario
     * <p>
     * <b>Nota: O usuário ROOT tem acesso ao registro de todos os usuários</b>
     *
     * @param usuario
     *            {@link String} usuario que deseja listar as atividades
     * @return {@link String} Texto com as atividades
     */
    public String listar(final String usuario) {
	final StringBuilder registros = new StringBuilder();
	try {
	    final List<String> linhas = FileUtils.readLines(arquivo);
	    for (final String linha : linhas) {
		final String[] chaveValor = linha
			.split(separadorUsuarioRegistro);
		final String usuarioAtividade = chaveValor[0];
		final String registroAtividade = chaveValor[1];

		if (ROOT.equalsIgnoreCase(usuario)) {
		    registros.append(registroAtividade)
		    .append(separadorDeLinha);
		} else if (usuario.equalsIgnoreCase(usuarioAtividade)) {
		    registros.append(registroAtividade)
		    .append(separadorDeLinha);
		}
	    }
	} catch (final IOException e) {
	    throw new RuntimeException(e);
	}

	// remover o ultimo separador de linha
	registros.replace(registros.lastIndexOf(separadorDeLinha),
		registros.length(), "");

	return registros.toString();
    }

    /**
     * Limpa todas as atividades já registradas
     */
    public void limparTodasAtividades() {
	FileUtils.deleteQuietly(arquivo);
    }
}
