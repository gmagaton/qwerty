package br.com.qwerty.seguranca;

import org.apache.commons.lang3.StringUtils;

import br.com.qwerty.tradutor.TradutorDeMensagem;

/**
 * Autenticador de usuário
 * <p>
 * Para obter sucesso na autenticação é necessário que a senha seja o código
 * correspondente ao texto
 *
 * <p>
 * Exemplo: usuário JOAO a senha será 56662666
 *
 * @author Gabriel
 *
 */
public class Autenticador {

    private final TradutorDeMensagem tradutorDeMensagem = new TradutorDeMensagem();

    /**
     * Autentica o usuário com a senha informada
     *
     * @param usuario
     *            {@link String} com o nome do usuário
     * @param senha
     *            {@link String} com a senha do usuário
     * @throws AutenticarException
     *             Caso a senha não esteja correta
     */
    public void autenticar(final String usuario, final String senha)
	    throws AutenticarException {
	if (StringUtils.isBlank(usuario) || StringUtils.isBlank(senha)) {
	    throw new AutenticarException(
		    "Usuário / Senha devem ser informados");
	}

	final String codigo = tradutorDeMensagem.traduzir(usuario);
	if (!codigo.equals(senha)) {
	    throw new AutenticarException("Usuário ou Senha inválido");
	}
    }

}
