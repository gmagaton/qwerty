package br.com.qwerty.seguranca;


/**
 * Exceção para tratamento de erro na autenticação do usuário
 *
 * @author Gabriel
 *
 */
public class AutenticarException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Constroi a execeção com o motivo
     *
     * @param erro
     *            {@link String} descrição do motivo do erro
     */
    public AutenticarException(final String erro) {
	super(erro);
    }

}
