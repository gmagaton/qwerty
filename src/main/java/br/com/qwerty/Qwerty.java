package br.com.qwerty;

import org.apache.commons.lang3.StringUtils;

import br.com.qwerty.rastreamento.RastreadorDeAtividade;
import br.com.qwerty.seguranca.Autenticador;
import br.com.qwerty.seguranca.AutenticarException;
import br.com.qwerty.tradutor.TradutorDeMensagem;

/**
 * Executor de atividades da linha de comando
 *
 * @author Gabriel
 *
 */
public class Qwerty {

    private final Autenticador autenticador = new Autenticador();
    private final RastreadorDeAtividade rastreadorDeAtividade = new RastreadorDeAtividade();
    private final TradutorDeMensagem tradutorDeMensagem = new TradutorDeMensagem();

    public void autenticarUsuario(final String usuario, final String senha) {
	try {
	    autenticador.autenticar(usuario, senha);
	} catch (final AutenticarException e) {
	    System.err.println(e.getMessage());
	}
    }

    public void exibirAtividadesDoUsuario(final String usuario) {
	final String atividade = rastreadorDeAtividade.listar(usuario);
	System.out.println("Registro de Atividades");
	System.out.print("Usuário: ");
	System.out.println(usuario);
	System.out.println("Atividades: ");
	System.out.println(atividade);
    }

    public void registrarAtividadeDoUsuario(final String usuario,
	    final String mensagemOuCodigo) {
	rastreadorDeAtividade.registrar(usuario, mensagemOuCodigo);
    }

    public void traduzir(final String mensagemOuCodigo) {
	final String mensagemTraduzida = tradutorDeMensagem
		.traduzir(mensagemOuCodigo);
	if (StringUtils.isBlank(mensagemOuCodigo)
		|| StringUtils.isBlank(mensagemTraduzida)) {
	    return;
	}
	System.out.print("Texto Original: ");
	System.out.println(mensagemOuCodigo);
	System.out.print("Texto Traduzido: ");
	System.out.println(mensagemTraduzida);
    }
}
