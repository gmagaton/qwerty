package br.com.qwerty;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Responsável por receber os argumentos via linha de comando e repassar ao
 * executor os comandos {@link Qwerty}
 *
 * @author Gabriel
 *
 */
public class QwertyMain {

    public static void main(final String[] args) throws ParseException {
	// possiveis opcoes
	final Options opcoes = new Options();
	final Option opcaoUsuario = new Option("u", true,
		"Usuário de autenticação");
	final Option opcaoSenha = new Option("s", true, "Senha de autenticação");
	final Option opcaoTexto = new Option("t", true,
		"Texto que deseja traduzir");
	final Option opcaoLista = new Option("r", false,
		"Listar todo o registro de atividade do usuário");

	// adiciona as opcoes
	opcoes.addOption(opcaoUsuario);
	opcoes.addOption(opcaoSenha);
	opcoes.addOption(opcaoTexto);
	opcoes.addOption(opcaoLista);

	final CommandLineParser parser = new GnuParser();
	final CommandLine cmd = parser.parse(opcoes, args);

	// help de opcoes
	if (cmd.getOptions().length <= 0) {
	    final HelpFormatter formatter = new HelpFormatter();
	    formatter.printHelp("help", opcoes);
	    return;
	}

	// recuperar valor das opcoes
	final String usuario = cmd.getOptionValue(opcaoUsuario.getOpt());
	final String senha = cmd.getOptionValue(opcaoSenha.getOpt());
	final String mensagemOuCodigo = cmd.getOptionValue(opcaoTexto.getOpt());

	// execucao
	final Qwerty qwerty = new Qwerty();
	qwerty.autenticarUsuario(usuario, senha);

	final boolean opcaoListarAtividadesDoUsuario = cmd.hasOption(opcaoLista
		.getOpt());
	if (opcaoListarAtividadesDoUsuario) {
	    qwerty.exibirAtividadesDoUsuario(usuario);
	} else {
	    qwerty.registrarAtividadeDoUsuario(usuario, mensagemOuCodigo);
	    qwerty.traduzir(mensagemOuCodigo);
	}

    }

}
