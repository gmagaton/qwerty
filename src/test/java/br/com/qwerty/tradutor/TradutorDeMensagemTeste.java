package br.com.qwerty.tradutor;

import org.junit.Assert;
import org.junit.Test;

/**
 * Teste de tradutor de mensagem de texto para sequência numérica e da sequência
 * numérica para a mensagem
 *
 * @author Gabriel
 *
 */
public class TradutorDeMensagemTeste {

    private final TradutorDeMensagem tradutor = new TradutorDeMensagem();

    @Test
    public void deveTraduzirAlfabeto() {
	Assert.assertEquals("0", tradutor.traduzir(" "));
	Assert.assertEquals("2", tradutor.traduzir("A"));
	Assert.assertEquals("22", tradutor.traduzir("B"));
	Assert.assertEquals("222", tradutor.traduzir("C"));
	Assert.assertEquals("3", tradutor.traduzir("D"));
	Assert.assertEquals("33", tradutor.traduzir("E"));
	Assert.assertEquals("333", tradutor.traduzir("F"));
	Assert.assertEquals("4", tradutor.traduzir("G"));
	Assert.assertEquals("44", tradutor.traduzir("H"));
	Assert.assertEquals("444", tradutor.traduzir("I"));
	Assert.assertEquals("5", tradutor.traduzir("J"));
	Assert.assertEquals("55", tradutor.traduzir("K"));
	Assert.assertEquals("555", tradutor.traduzir("L"));
	Assert.assertEquals("6", tradutor.traduzir("M"));
	Assert.assertEquals("66", tradutor.traduzir("N"));
	Assert.assertEquals("666", tradutor.traduzir("O"));
	Assert.assertEquals("7", tradutor.traduzir("P"));
	Assert.assertEquals("77", tradutor.traduzir("Q"));
	Assert.assertEquals("777", tradutor.traduzir("R"));
	Assert.assertEquals("7777", tradutor.traduzir("S"));
	Assert.assertEquals("8", tradutor.traduzir("T"));
	Assert.assertEquals("88", tradutor.traduzir("U"));
	Assert.assertEquals("888", tradutor.traduzir("V"));
	Assert.assertEquals("9", tradutor.traduzir("W"));
	Assert.assertEquals("99", tradutor.traduzir("X"));
	Assert.assertEquals("999", tradutor.traduzir("Y"));
	Assert.assertEquals("9999", tradutor.traduzir("Z"));
    }

    @Test
    public void deveTraduzirCodigo() {
	Assert.assertEquals(" ", tradutor.traduzir("0"));
	Assert.assertEquals("A", tradutor.traduzir("2"));
	Assert.assertEquals("B", tradutor.traduzir("22"));
	Assert.assertEquals("C", tradutor.traduzir("222"));
	Assert.assertEquals("D", tradutor.traduzir("3"));
	Assert.assertEquals("E", tradutor.traduzir("33"));
	Assert.assertEquals("F", tradutor.traduzir("333"));
	Assert.assertEquals("G", tradutor.traduzir("4"));
	Assert.assertEquals("H", tradutor.traduzir("44"));
	Assert.assertEquals("I", tradutor.traduzir("444"));
	Assert.assertEquals("J", tradutor.traduzir("5"));
	Assert.assertEquals("K", tradutor.traduzir("55"));
	Assert.assertEquals("L", tradutor.traduzir("555"));
	Assert.assertEquals("M", tradutor.traduzir("6"));
	Assert.assertEquals("N", tradutor.traduzir("66"));
	Assert.assertEquals("O", tradutor.traduzir("666"));
	Assert.assertEquals("P", tradutor.traduzir("7"));
	Assert.assertEquals("Q", tradutor.traduzir("77"));
	Assert.assertEquals("R", tradutor.traduzir("777"));
	Assert.assertEquals("S", tradutor.traduzir("7777"));
	Assert.assertEquals("T", tradutor.traduzir("8"));
	Assert.assertEquals("U", tradutor.traduzir("88"));
	Assert.assertEquals("V", tradutor.traduzir("888"));
	Assert.assertEquals("W", tradutor.traduzir("9"));
	Assert.assertEquals("X", tradutor.traduzir("99"));
	Assert.assertEquals("Y", tradutor.traduzir("999"));
	Assert.assertEquals("Z", tradutor.traduzir("9999"));
    }

    @Test
    public void deveTraduzirMensagem() {
	Assert.assertEquals("833777783303_33063377772",
		tradutor.traduzir("TESTE DE MESA"));
    }

}
