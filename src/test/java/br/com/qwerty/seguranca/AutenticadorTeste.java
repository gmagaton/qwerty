package br.com.qwerty.seguranca;

import org.junit.Assert;
import org.junit.Test;

/**
 * Teste de autenticação
 *
 * @author Gabriel
 *
 */
public class AutenticadorTeste {

    private final Autenticador autenticador = new Autenticador();

    @Test
    public void deveAutenticarUsuarioESenhaCorreto() {
	try {
	    autenticador.autenticar("GABRIEL", "42_2277744433555");
	} catch (final AutenticarException e) {
	    Assert.fail(e.getMessage());
	}
    }

    @Test(expected = AutenticarException.class)
    public void naoDeveAutenticarUsuarioOuSenhaInvalido()
	    throws AutenticarException {
	autenticador.autenticar("GABRIEL", "GABRIEL");
    }

}
