package br.com.qwerty.rastreamento;

import org.junit.Assert;
import org.junit.Test;

/**
 * Teste de rastreamento de atividades
 *
 * @author Gabriel
 *
 */
public class RastreadorDeAtividadeTeste {

    private final RastreadorDeAtividade rastreador = new RastreadorDeAtividade();

    public void inicializar() {
	rastreador.limparTodasAtividades();
	rastreador.registrar("GABRIEL", "MENSAGEM GABRIEL");
	rastreador.registrar("GUSTAVO", "MENSAGEM GUSTAVO");
	rastreador.registrar("ROOT", "MENSAGEM ROOT");
    }

    @Test
    public void deveRegistrarAtividade() {
	rastreador.registrar("JOAO", "MENSAGEM JOAO");
	final String atividades = rastreador.listar("JOAO");
	Assert.assertTrue(atividades.contains("MENSAGEM JOAO"));
    }

    @Test
    public void deveListarAtividadesDoUsuario() {
	inicializar();
	final String atividades = rastreador.listar("GABRIEL");
	Assert.assertEquals("MENSAGEM GABRIEL", atividades);
    }

    @Test
    public void deveListarTodasAtividades() {
	inicializar();
	final String atividades = rastreador.listar("ROOT");
	final StringBuilder atividadesEsperadas = new StringBuilder();
	atividadesEsperadas.append("MENSAGEM GABRIEL").append(
		System.getProperty("line.separator"));
	atividadesEsperadas.append("MENSAGEM GUSTAVO").append(
		System.getProperty("line.separator"));
	atividadesEsperadas.append("MENSAGEM ROOT");
	Assert.assertEquals(atividadesEsperadas.toString(), atividades);
    }
}
