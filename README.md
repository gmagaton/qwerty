# Leia-me #

Aplicação de Desafio do processo seletivo da Ci&T.

Como utilizar ?

Exemplo: 
       java -jar qwerty.jar -u GABRIEL -s 42_2277744433555 -t MENSAGEM
       java -jar qwerty.jar -u GABRIEL -s 42_2277744433555 -t 42_2277744433555

Para listar os registros do usuário, utilizar o parâmetro -r, exemplo:
       java -jar qwerty.jar -u GABRIEL -s 42_2277744433555 -r

Obs:  O Usuário ROOT lista a atividades de todos usuários.
      

# DESAFIO #

Instruções

Desenvolver um programa conforme o desafio detalhado abaixo
Fique a vontade para desenvolver uma aplicação Web, Command Line ou Swing-like
Importante que o código do algoritmo seja Java. Caso opte por uma interface Web o algoritmo não poderá ser escrito em JavaScript no Client-Side e sim em Java no Server-Side.
Commitar o código no bitbucket.org: dar acesso para os seguintes usuários rrocha_cit, bsimioni_cit, lvendramin_cit, marceloa_cit e mballoni_cit
O último commit válido deverá ser até no máximo 24 horas após você receber este desafio.
Considere utilizar boas práticas de desenvolvimento de software como TDD ou BDD
Considere implementar os seguintes requisitos de segurança:
A aplicação deverá ser segura
A aplicação deverá ter o rastreamento do que o usuário realizou no sistema.

Desafio

Um dos serviços mais utilizados pelos usuários de aparelhos celulares são os SMS (Short Message Service), que permite o envio de mensagens curtas (até 255 caracteres em redes GSM e 160 caracteres em redes CDMA).

Para digitar uma mensagem em um aparelho que não possui um teclado QWERTY embutido é necessário fazer algumas combinações das 10 teclas numéricas do aparelho para conseguir digitar. Cada número é associado a um conjunto de letras como a seguir:

Letras  ->  Número 
ABC    ->  2 
DEF    ->  3 
GHI    ->  4 
JKL    ->  5 
MNO    ->  6 
PQRS    ->  7 
TUV    ->  8 
WXYZ   ->  9 
Espaço -> 0
 
Desenvolva um programa que:
Dada uma mensagem de texto limitada a 255 caracteres, retorne a seqüência de números que precisa ser digitada. 
Dada uma sequencia de números retorne o texto

Caso uma sequencia use a mesma tecla, deve ser usada “_” para separar.

Por exemplo, para digitar "TESTE DE MESA", você precisa digitar:
833777783303_33063377772